const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user.js');
const recipeSchema = require('./models/recipe.js');
const recipe = require('./models/recipe.js');



const app = express();


//middleware
app.use(express.json());

//MongoDB

mongoose.connect('mongodb://poli:password@mongoRecetas2:27017/miapp?authSource=admin')

.then( () => console.log("Conectado a Mongo")  )
.catch(  (error) => console.log(error) ) //Es para saber el error que ocurre 

app.get('/', (req, res) =>  {  // El '/' es el url localhost:3000

    res.send("Hola Daniel Ferszt, bienvenido a mi API de recetas");   

} );


app.post('/new_user', (req, res) =>  {  

    const user = userSchema(req.body);

    user.save()
    .then( (data) => res.json(data)) //Cuando se termine de grabar..., data es la respuesta de mongo
    .catch( (error) => res.send(error));

} )


app.post('/new_recipe', (req,res) => {

    const recipe = recipeSchema(req.body);

    recipe.save()
    .then( (data) => res.json(data)) //Cuando se termine de grabar..., data es la respuesta de mongo
    .catch( (error) => res.send(error));
})

app.post('/rate', (req,res) => {

    const { recipeId, userId, rating } = req.body //Obtenemos los datos

    recipeSchema.updateOne(

        { _id: recipeId },
        
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            { $set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
        ]

        
    )
    .then( (data) => res.json(data)) //Cuando se termine de grabar..., data es la respuesta de mongo
    .catch( (error) => {

        console.log()
        res.send(error)
        
    });

   
})

app.get('/recipes', (req,res) => {

    const {userId,recipeId} = req.body

    if (recipeId){  //Si tenemos algun recipeID...

        recipeSchema
        .find(  { _id:recipeId}  )
        .then( (data) => res.json(data))
        .catch( (error) => res.json({message:error}))
    } else {

        recipeSchema  //Si tenemos algun UserID...
        .find(  { userId:userId}  )
        .then( (data) => res.json(data))
        .catch( (error) => res.json({message:error}))

    }

})


// ----------------------------------------------------

app.get('/recipesbyingredient', async (req,res) => {

    const list_ingredients = req.body.ingredients.map((ingredient) => ingredient.name);

    recipeSchema.aggregate ([

        {$set: {es_subset: {$setIsSubset: ["$ingredients.name", list_ingredients]}}},
        {$match: {es_subset: true }},
        {$project: {es_subset:0}}        

    ])
    .exec()
    .then(resultados => {
        res.json(resultados)
    })
    .catch(err => {
        console.log(err)
    })


 
})



app.listen(3000, () => console.log("Escuchando en el puerto 3000"));