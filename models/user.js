const mongoose = require('mongoose');

const userSchema = mongoose.Schema({

    schema: {

        type: Number,
        required: true,

    },

    name: {

        type: String,
        required: true,
        lowercase: true,  //Que necesariamente sea en minusculas

    }


})

module.exports = mongoose.model('User',userSchema)  //Nos permite exportar de este archivo algo que otro archivo pueda usar